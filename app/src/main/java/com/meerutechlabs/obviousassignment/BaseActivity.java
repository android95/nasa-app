package com.meerutechlabs.obviousassignment;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.meerutechlabs.obviousassignment.utils.Constants;
import com.meerutechlabs.obviousassignment.utils.Tools;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

public abstract class BaseActivity extends AppCompatActivity {
    public ProgressBar progressBar;
    ActionBar actionBar;
    FrameLayout rootParentLayout;

    @Override
    public void setContentView(int layoutResID) {
        ConstraintLayout constraintLayout = (ConstraintLayout) getLayoutInflater().inflate(R.layout.activity_base,null);
        rootParentLayout  = constraintLayout.findViewById(R.id.activity_content);
        progressBar = constraintLayout.findViewById(R.id.progress_bar);
        getLayoutInflater().inflate(layoutResID,rootParentLayout,true);
        super.setContentView(constraintLayout);
    }

    public void saveContext(Activity activity){
        Constants.CONTEXT = activity;
    }

    public void hideContent(){
        rootParentLayout.setVisibility(View.INVISIBLE);
    }

    public void showContent(){
        rootParentLayout.setVisibility(View.VISIBLE);
    }

    public void initToolBar(Activity activity){
        Toolbar toolbar = (Toolbar) activity.findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
        setToolbarTitle();
        removeToolbarNavigation();
    }

    public void removeToolBarTitle(){
        if(actionBar == null){
            initToolBar((Activity) Constants.CONTEXT);
        }
        actionBar.setTitle("");
    }

    public void showToolbarNavigation(){
        if(actionBar == null){
            initToolBar((Activity) Constants.CONTEXT);
        }
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }

    public void removeToolbarNavigation(){
        if(actionBar == null){
            initToolBar((Activity) Constants.CONTEXT);
        }
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);
    }

    public void setToolbarTitle(){
        if(actionBar == null){
            initToolBar((Activity) Constants.CONTEXT);
        }
        actionBar.setTitle(getApplicationName());
    }

    public static String getApplicationName() {
        ApplicationInfo applicationInfo = Constants.CONTEXT.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : Constants.CONTEXT.getString(stringId);
    }

    public void initStatusBarColor(Activity activity){
        Tools.setSystemBarColor(activity, R.color.colorPrimaryDark);
    }

    public void showProgressBar(boolean visibility){
        progressBar.setVisibility(visibility ? View.VISIBLE : View.INVISIBLE);
    }
}