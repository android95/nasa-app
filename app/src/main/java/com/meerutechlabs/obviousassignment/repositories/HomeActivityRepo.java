package com.meerutechlabs.obviousassignment.repositories;

import com.meerutechlabs.obviousassignment.api.NasaApiClient;
import com.meerutechlabs.obviousassignment.models.ImageModel;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class HomeActivityRepo {
    private static HomeActivityRepo homeActivityRepo;
    private NasaApiClient nasaApiClient;

    private HomeActivityRepo() {
        nasaApiClient = NasaApiClient.getInstance();
    }

    public static HomeActivityRepo getInstance(){
        if(homeActivityRepo == null){
            homeActivityRepo = new HomeActivityRepo();
        }
        return homeActivityRepo;
    }

    public LiveData<List<ImageModel>> getImageList(){
        return nasaApiClient.getImageList();
    }

    public void showImageDetails(ImageModel imageModel) {
        nasaApiClient.showImageDetails(imageModel);
    }
}
