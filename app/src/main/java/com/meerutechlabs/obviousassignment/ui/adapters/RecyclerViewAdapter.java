package com.meerutechlabs.obviousassignment.ui.adapters;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.meerutechlabs.obviousassignment.R;
import com.meerutechlabs.obviousassignment.models.ImageModel;
import com.meerutechlabs.obviousassignment.models.ImageType;
import java.util.ArrayList;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ImageModel> imageModelList;
    private OnClickEventListener onClickEventListener;
    public static final int VIEW_TYPE_IMAGE = 0;
    public static final int VIEW_TYPE_DETAILED = 1;
    public static final int VIEW_TYPE_PROGRESS_BAR = 2;

    public RecyclerViewAdapter(OnClickEventListener onClickEventListener) {
        this.onClickEventListener = onClickEventListener;
    }

    public void setAdapterList(List<ImageModel> imageModelList){
        this.imageModelList = new ArrayList<>();
        this.imageModelList.addAll(imageModelList);
        notifyDataSetChanged();
    }

    public List<ImageModel> getAdapterList(){
        return imageModelList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = null;
        if(viewType==VIEW_TYPE_IMAGE){
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, null);
            return new ViewHolderImage(v,onClickEventListener);
        }
        else if(viewType == VIEW_TYPE_DETAILED){
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_detailed, null);
            return new ViewHolderDetailed(v,onClickEventListener);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ViewHolderImage){
            setImageViewHolder((ViewHolderImage) holder,position);
        }
        else if(holder instanceof ViewHolderDetailed){
            setImageDetails((ViewHolderDetailed) holder,position);
        }
    }

    private void setImageViewHolder(final ViewHolderImage h, int i){
        RequestOptions options = new RequestOptions()
                .error(R.drawable.ic_launcher_background);

        Glide.with(h.itemView.getContext())
                .setDefaultRequestOptions(options)
                .load(imageModelList.get(i).getImgURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }
                    @Override
                    public boolean onResourceReady(Drawable resource, Object model,
                                                   Target<Drawable> target, DataSource dataSource,
                                                   boolean isFirstResource) {
                        h.progressBar.setVisibility(View.INVISIBLE);
                        return false;
                    }
                })
                .into(h.imageView);
    }

    private void setImageDetails(final ViewHolderDetailed h, final int i){
        h.progressBar.setVisibility(View.VISIBLE);
        RequestOptions options = new RequestOptions()
                .error(R.drawable.ic_launcher_background);
        Glide.with(h.itemView.getContext())
                .setDefaultRequestOptions(options)
                .load(imageModelList.get(i).getImgURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }
                    @Override
                    public boolean onResourceReady(Drawable resource, Object model,
                                                   Target<Drawable> target, DataSource dataSource,
                                                   boolean isFirstResource) {
                        h.textView.setText(imageModelList.get(i).getTitle());
                        if(imageModelList.get(i).getCopyrightInfo() != null){
                            h.textViewCopyRight.setText(imageModelList.get(i).getCopyrightInfo());
                        }
                        h.textViewExplanation.setText(imageModelList.get(i).getExplanation());
                        h.textViewDate.setText(imageModelList.get(i).getDate());
                        h.progressBar.setVisibility(View.INVISIBLE);
                        return false;
                    }
                })
                .into(h.imageView);
    }

    @Override
    public int getItemViewType(int i) {
        switch(imageModelList.get(i).getType()) {
            case IMAGE_VIEW:
                return VIEW_TYPE_IMAGE;
            case IMAGE_DETAILS:
                return VIEW_TYPE_DETAILED;
            default:
                return VIEW_TYPE_PROGRESS_BAR;
        }
    }

    @Override
    public int getItemCount() {
        if(imageModelList != null){
            return imageModelList.size();
        }
        else{
            return 0;
        }
    }
}
