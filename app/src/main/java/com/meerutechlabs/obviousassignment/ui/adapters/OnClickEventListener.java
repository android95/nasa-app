package com.meerutechlabs.obviousassignment.ui.adapters;

public interface OnClickEventListener {
    public void onImageClicked(int position);
}
