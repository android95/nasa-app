package com.meerutechlabs.obviousassignment.ui.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.meerutechlabs.obviousassignment.R;
import com.meerutechlabs.obviousassignment.models.ImageModel;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ViewHolderImage extends RecyclerView.ViewHolder implements View.OnClickListener {
    private OnClickEventListener onClickEventListener;
    public ImageView imageView;
    public ProgressBar progressBar;

    public ViewHolderImage(@NonNull View itemView, OnClickEventListener onClickEventListener) {
        super(itemView);
        this.onClickEventListener = onClickEventListener;
        this.imageView = (ImageView) itemView.findViewById(R.id.imageView);
        this.progressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
        imageView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.imageView){
            this.onClickEventListener.onImageClicked(getAdapterPosition());
        }
    }
}
