package com.meerutechlabs.obviousassignment.ui.adapters;

import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.meerutechlabs.obviousassignment.R;
import com.meerutechlabs.obviousassignment.utils.Constants;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ViewHolderDetailed extends RecyclerView.ViewHolder {
    public TextView textView;
    public TextView textViewCopyRight;
    public TextView textViewExplanation;
    public ImageView imageView;
    public ProgressBar progressBar;
    public TextView textViewDate;

    OnClickEventListener onClickEventListener;

    public ViewHolderDetailed(@NonNull View itemView,OnClickEventListener onClickEventListener) {
        super(itemView);
        textView = itemView.findViewById(R.id.textView);
        imageView = itemView.findViewById(R.id.imageView);
        progressBar = itemView.findViewById(R.id.progressBar2);
        textViewCopyRight = itemView.findViewById(R.id.textViewAuthor);
        textViewExplanation = itemView.findViewById(R.id.textViewExplaination);
        textViewDate = itemView.findViewById(R.id.textViewDate);
        this.onClickEventListener = onClickEventListener;
        Typeface courgette = Typeface.createFromAsset(Constants.CONTEXT.getAssets(), "fonts/Courgette-Regular.ttf");
        textView.setTypeface(courgette);
        Typeface robotoRegular = Typeface.createFromAsset(Constants.CONTEXT.getAssets(), "fonts/Roboto-Light.ttf");
        textViewExplanation.setTypeface(robotoRegular);
    }
}
