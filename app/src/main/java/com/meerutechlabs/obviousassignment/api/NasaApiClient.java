package com.meerutechlabs.obviousassignment.api;

import android.os.Handler;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.meerutechlabs.obviousassignment.models.ImageModel;
import com.meerutechlabs.obviousassignment.models.ImageType;
import com.meerutechlabs.obviousassignment.utils.Constants;
import com.meerutechlabs.obviousassignment.utils.JsonReader;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class NasaApiClient {
    private MutableLiveData<List<ImageModel>> mImageList;
    private static NasaApiClient apiClient;

    private NasaApiClient() {
        mImageList = new MutableLiveData<>();
    }

    public static NasaApiClient getInstance(){
        if(apiClient == null){
            apiClient = new NasaApiClient();
        }
        return apiClient;
    }

    public LiveData<List<ImageModel>> getImageList(){
        prepareDataList();
        return this.mImageList;
    }

    public void showImageDetails(ImageModel imageModel) {
        List<ImageModel> imageModelList = new ArrayList<>();
        imageModel.setType(ImageType.IMAGE_DETAILS);
        imageModelList.add(imageModel);

//        imageModelList.add(imageModel);
        mImageList.setValue(imageModelList);
    }

    public void prepareDataList(){
        try {
            if(Constants.CONTEXT != null){
                final List<ImageModel> imageModelList = new ArrayList<>();
                Gson gson = new Gson();
                JsonArray imageJsonArray = (JsonArray) JsonParser.
                        parseString(JsonReader.readJSON("data/data.json",Constants.CONTEXT));
                for(JsonElement jsonElement : imageJsonArray){
                    ImageModel imageModel = gson.fromJson(jsonElement,ImageModel.class);
                    imageModelList.add(imageModel);
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mImageList.setValue(imageModelList);
                    }
                },1500);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
