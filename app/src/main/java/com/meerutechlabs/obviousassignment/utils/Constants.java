package com.meerutechlabs.obviousassignment.utils;

import android.content.Context;

public class Constants {
    public static final String DATA_PATH = "";
    public static final String BASE_URL = "https://recipesapi.herokuapp.com/";
    public static final int PAGINATION_SIZE = 6;
    public static Context CONTEXT = null;
}
