package com.meerutechlabs.obviousassignment.viewmodels;

import com.meerutechlabs.obviousassignment.models.ImageModel;
import com.meerutechlabs.obviousassignment.repositories.HomeActivityRepo;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class HomeActivityViewModel extends ViewModel {
    private HomeActivityRepo homeActivityRepo;
    private boolean isImageDetailsShowing;

    public HomeActivityViewModel(){
        homeActivityRepo = HomeActivityRepo.getInstance();
        isImageDetailsShowing = false;
    }

    public LiveData<List<ImageModel>> getImageList(){
        return homeActivityRepo.getImageList();
    }

    public void showImageDetails(ImageModel imageModel) {
        homeActivityRepo.showImageDetails(imageModel);
        isImageDetailsShowing = true;
    }

    public boolean onBackPressed(){
        if(isImageDetailsShowing){
            isImageDetailsShowing = false;
            return false;
        }
        return true;
    }
}
