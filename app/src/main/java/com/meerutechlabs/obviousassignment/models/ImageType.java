package com.meerutechlabs.obviousassignment.models;

public enum ImageType {
    IMAGE_VIEW,
    IMAGE_DETAILS,
    LOADING_PROGRESS
}
