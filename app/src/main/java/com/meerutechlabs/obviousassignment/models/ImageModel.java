package com.meerutechlabs.obviousassignment.models;

import com.google.gson.annotations.SerializedName;

public class ImageModel {
    private String date;
    @SerializedName("copyright")
    private String copyrightInfo;
    private String title;
    private String explanation;
    private String service_version;
    @SerializedName("url")
    private String imgURL;
    @SerializedName("hdurl")
    private String imgURLHD;
    private String media_type;
    private ImageType type = ImageType.IMAGE_VIEW;

    public ImageModel(){
    }

    public ImageModel(String date, String copyrightInfo, String title, String explanation, String service_version, String imgURL, String imgURLHD, String media_type) {
        this.date = date;
        this.copyrightInfo = copyrightInfo;
        this.title = title;
        this.explanation = explanation;
        this.service_version = service_version;
        this.imgURL = imgURL;
        this.imgURLHD = imgURLHD;
        this.media_type = media_type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCopyrightInfo() {
        return copyrightInfo;
    }

    public void setCopyrightInfo(String copyrightInfo) {
        this.copyrightInfo = copyrightInfo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public String getService_version() {
        return service_version;
    }

    public void setService_version(String service_version) {
        this.service_version = service_version;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getImgURLHD() {
        return imgURLHD;
    }

    public void setImgURLHD(String imgURLHD) {
        this.imgURLHD = imgURLHD;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public ImageType getType() {
        return type;
    }

    public void setType(ImageType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ImageModel{" +
                "date='" + date + '\'' +
                ", copyrightInfo='" + copyrightInfo + '\'' +
                ", title='" + title + '\'' +
                ", explanation='" + explanation + '\'' +
                ", service_version='" + service_version + '\'' +
                ", imgURL='" + imgURL + '\'' +
                ", imgURLHD='" + imgURLHD + '\'' +
                ", media_type='" + media_type + '\'' +
                ", type=" + type +
                '}';
    }
}
