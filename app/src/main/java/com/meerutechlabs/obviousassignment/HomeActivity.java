package com.meerutechlabs.obviousassignment;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.meerutechlabs.obviousassignment.models.ImageModel;
import com.meerutechlabs.obviousassignment.ui.adapters.OnClickEventListener;
import com.meerutechlabs.obviousassignment.ui.adapters.RecyclerViewAdapter;
import com.meerutechlabs.obviousassignment.utils.SpacingItemDecoration;
import com.meerutechlabs.obviousassignment.utils.Tools;
import com.meerutechlabs.obviousassignment.viewmodels.HomeActivityViewModel;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

public class HomeActivity extends BaseActivity implements OnClickEventListener {
    RecyclerView recyclerView;
    RecyclerViewAdapter recyclerViewAdapter;
    HomeActivityViewModel homeActivityViewModel;
    LinearLayoutManager linearLayoutManager;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    SpacingItemDecoration spacingItemDecoration;
    GridLayoutManager gridLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        saveContext(this);
        initComponents();
        initViewModel();
        initObservables();
    }

    private void initComponents() {
        initRecyclerView();
        initToolBar(this);
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView);
        spacingItemDecoration =  new SpacingItemDecoration(2,
                Tools.dpToPx(this, 8),true);
        recyclerView.addItemDecoration(spacingItemDecoration);
        recyclerViewAdapter = new RecyclerViewAdapter(this);
        gridLayoutManager = new GridLayoutManager(this, 2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (recyclerViewAdapter.getItemViewType(position)) {
                    case RecyclerViewAdapter.VIEW_TYPE_DETAILED:
                        return 2;
                    default:
                        return 1;
                }
            }
        });
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    private void initViewModel() {
        homeActivityViewModel = new ViewModelProvider(this).get(HomeActivityViewModel.class);
    }

    private void initObservables() {
        showProgressBar(true);
        homeActivityViewModel.getImageList().observe(this, new Observer<List<ImageModel>>() {
            @Override
            public void onChanged(List<ImageModel> imageModelList) {
                if(imageModelList.size()==1 && recyclerView.getItemDecorationCount() ==1){
                    recyclerView.removeItemDecorationAt(0);
                    removeToolBarTitle();
                    showToolbarNavigation();
                }
                else{
                    if(recyclerView.getItemDecorationCount()==0){
                        recyclerView.addItemDecoration(spacingItemDecoration);
                    }
                }
                recyclerViewAdapter.setAdapterList(imageModelList);
                showProgressBar(false);
                showContent();
            }
        });
    }

    @Override
    public void onImageClicked(int position) {
        showProgressBar(true);
        ImageModel imageModel = recyclerViewAdapter.getAdapterList()
                .get(position);
        homeActivityViewModel.showImageDetails(imageModel);
    }

    @Override
    public void onBackPressed() {
        if(homeActivityViewModel.onBackPressed()){
            super.onBackPressed();
        }
        else{
            initToolBar(this);
            hideContent();
            showProgressBar(true);
            homeActivityViewModel.getImageList();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}